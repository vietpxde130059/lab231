USE [J3.L.P0015]
GO
/****** Object:  Table [dbo].[contact]    Script Date: 11/2/2019 7:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[name] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[tel] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[myCake]    Script Date: 11/2/2019 7:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[myCake](
	[name] [nvarchar](50) NULL,
	[image] [nvarchar](max) NULL,
	[detail] [nvarchar](max) NULL,
	[role] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[contact] ([name], [address], [tel], [email]) VALUES (N'Maria Bagnarelli', N'Copenhagen, Denmark', N'123456', N'your-email@your-email.com')
INSERT [dbo].[myCake] ([name], [image], [detail], [role]) VALUES (N'Maria''s Cosy Cafe', N'./Home - www.simplesite.com_us-123cafe_files/i282319414584937113._szw480h1280_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica', N'pro')
INSERT [dbo].[myCake] ([name], [image], [detail], [role]) VALUES (N'In the Afternoon', N'./Home - www.simplesite.com_us-123cafe_files/i282319414620354139._rsw480h360_szw480h360_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', N'pro')
INSERT [dbo].[myCake] ([name], [image], [detail], [role]) VALUES (N'Traditional Cakes', N'./Home - www.simplesite.com_us-123cafe_files/i282319414620354374._rsw480h360_szw480h360_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

', N'pro')
INSERT [dbo].[myCake] ([name], [image], [detail], [role]) VALUES (N'About my Cakes', N'./About my Cakes - www.simplesite.com_us-123cafe_files/i282319414620354444._szw480h1280_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.

Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.', N'about')
