
<%@page import="java.util.ArrayList"%>
<%@page import="model.myCake"%>
<script type="text/javascript">
    var thisDomain = '';
    if (thisDomain.length > 0) {
        document.domain = thisDomain;
    }
</script>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <title>About my Cakes - www.simplesite.com/us-123cafe</title>
        <meta property="fb:app_id" content="1880640628839943" />
        <meta property="og:site_name" content="Maria Bagnarelli&#39;s Cafe" />
        <meta property="article:publisher" content="https://www.facebook.com/simplesite" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:url" content="http://www.simplesite.com/us-123cafe/118937148" />
        <meta property="og:title" content="About my Cakes" />
        <meta property="og:description" content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip
              ex ea commodo consequat. Duis autem vel eum iriure ..." />
        <meta property="og:image:url" content="https://cdn.simplesite.com/i/63/0a/282319406567983715/i282319414620354444._szw1280h1280_.jpg" />
        <meta property="og:image" content="https://cdn.simplesite.com/i/63/0a/282319406567983715/i282319414620354444._szw1280h1280_.jpg" />
        <meta property="og:image:width" content="1280" />
        <meta property="og:image:height" content="1280" />
        <meta property="og:updated_time" content="2019-10-22T03:49:58.3793204Z" />
        <meta property="og:type" content="article" />
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="Maria Bagnarelli&#39;s Cafe - http://www.simplesite.com/us-123cafe/" />
        <link rel="stylesheet" type="text/css" href="//css.simplesite.com/e0/05/3746044.design.v25490.css?h=c43de4c35cd5975375525cb055b013d8e9ee9b96d5ec18bf5c6246cc5650661f" />
        <link rel="stylesheet" type="text/css" href="//css.simplesite.com/d/1567000755/designs/base/base.css" />
        <link rel="stylesheet" type="text/css" href="/d/designs/base/rowsconcept.css" />
        <link rel="stylesheet" type="text/css" href="/d/designs/base/somelinks.css" />
        <link rel="stylesheet" type="text/css" href="/d/designs/base/footerlayout5.css" />
        <link rel="stylesheet" type="text/css" href="/d/designs/base/quilljs.css" />
        <link rel="stylesheet" type="text/css" href="/Content/fontawesome-all.css" />
        <link rel="canonical" href="http://www.simplesite.com/us-123cafe/118937148" />
        <link rel="icon" sizes="194x194" href="/favicon-194x194.png">
        <link rel="stylesheet" type="text/css" href="/c/css/experiments/ionicons.css" />
        <script type="text/javascript" src="/userPages/pages/FrontendAppLocalePage.aspx?CultureKey=en-US"></script>
    </head>
    <body data-pid="118937148" data-iid="" style="line-height: normal;" class="stefan-asafti">


        <div class="container-fluid site-wrapper">
            <!-- this is the Sheet -->
            <div class="container-fluid header-wrapper " id="header"> <!-- this is the Header Wrapper -->
                <div class="container">
                    <div class="title-wrapper">
                        <div class="title-wrapper-inner">
                            <a class="logo " href="http://www.simplesite.com/us-123cafe/">
                            </a>
                            <div class="title ">
                                <a class="title  title-link" href="http://www.simplesite.com/us-123cafe/">
                                    Maria Bagnarelli&#39;s Cafe
                                </a> 
                            </div>
                            <div class="subtitle">
                                Welcome to my website
                            </div>
                        </div>
                    </div>  <!-- these are the titles -->
                    <div class="navbar navbar-compact">
                        <div class="navbar-inner">
                            <div class="container">
                                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse" title="Toggle menu">
                                    <span class="menu-name">Menu</span>
                                </a>



                                <!-- Everything you want hidden at 940px or less, place within here -->
                                <div class="nav-collapse collapse">
                                    <ul class="nav" id="topMenu" data-submenu="horizontal">
                                        <li class="  ">
                                            <a href="Home.jsp">Home</a>
                                        </li><li class=" active ">
                                            <a href="">About my Cakes</a>
                                        </li><li class="  ">
                                            <a href="Find Maria Cafe.jsp">Find Maria&#39;s Cafe</a>
                                        </li>                </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- this is the Menu content -->
                </div>
            </div>  <!-- this is the Header content -->
            <div class="container-fluid content-wrapper" id="content"> <!-- this is the Content Wrapper -->
                <div class="container">
                    <div class="row-fluid content-inner">
                        <div id="left" class="span9"> <!-- ADD "span12" if no sidebar, or "span9" with sidebar -->
                            <div class="wrapper ">
                                <div class="content">
                                    <div class="row-fluid layout5-row  margins-topbottom padding-all ">
                                        <div class="sections-wrapper">
                                            <div class="span12 ">
                                                <div class="outer-margin-on first last">
                                                    <div class="section article margins-on">
                                                        <style>    .wordwrapfix {
                                                                word-wrap:break-word;
                                                            }
                                                        </style>
                                                        <%
                                                            myCake m = new myCake();
                                                            ArrayList<myCake> al = m.getCake();
                                                            for (myCake x : al) {
                                                                if (x.getRole() != null && x.getRole().equals("about")) {

                                                        %>
                                                        <div class="heading wordwrapfix">
                                                            <h3><%=x.getName()%></h3>
                                                        </div>

                                                        <div class="content">
                                                            <div class="img-simple span6 pull-right">
                                                                <div class="image">
                                                                    <a data-ss="imagemodal" data-href="<%=x.getImage()%>" rel="group" has-arrows="False"><img src="<%=x.getImage()%>"></a>
                                                                </div>
                                                            </div>
                                                            <p><span><%=x.getDetail()%></span></p>    </div>
                                                    </div>
                                                    <%
                                                            }
                                                        }
                                                    %>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div id="right" class="span3  " >
                            <div class="sidebar">
                                <div class="wrapper share-box">
                                    <style>    .wordwrapfix {
                                            word-wrap:break-word;
                                        }
                                    </style>
                                    <div class="heading wordwrapfix">
                                        <h4>Share this page</h4>
                                    </div>

                                    <div class="content">
                                        <ul>
                                            <li><a id="share-facebook" href="#"><i class="icon-facebook-sign"></i><span>Share on Facebook</span></a></li>
                                            <li><a id="share-twitter" href="#"><i class="icon-twitter-sign"></i><span>Share on Twitter</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>  <!-- the controller has determined which view to be shown in the content -->
            <div class="container-fluid footer-wrapper" id="footer">
                <!-- this is the Footer Wrapper -->
                <div class="container">
                    <div class="footer-info">
                    </div>
                    <div class="footer-page-counter">
                        <i class="icon-spin icon-spinner"></i>
                    </div>
                    <div id="css_simplesite_com_fallback" class="hide"></div>
                </div>
            </div>

            <!-- this is the Footer content -->
        </div>


        <input type="hidden" id="anti-forgery-token" value="vFZIp11QDd7w1cZjYdzwVVw3rTHgQi1lpMMqomvLq/qGfx85dADIn6rd+jb5+yvdIjnIKNejlFu6vxL7J8Np5VEjkAOwXS4/ovXBJ0IQICH+0SDLST7xGy8P+X47RkaxI/rchghLTpcz1LAgRk6y61o4zizSqGZ6ZJYaRqSOcY3wgiUPMkgVxZVnqtxJ7GZ0jqug5iJAMTQc/BENBQzTKu1awUlW9U5bpjOmcFKL+pCe1deO/Oo9CNg/0QOZbW5Cg3p18XmTt8ayefTtJX6txRWEknHkJXsRFsJQmvvFgRTJZmhwVhJ7k0xLPAEp9wEJyBIpAbTZ0OwII/PcMm/xaGBWgX20Qo9KmAQdJG+1jirpu1gxFM1m7BXjPuVOUdZcFE5S6cSFdK0wuiq+WMN7fQzFaaIEdbJRnIudbxfF1mGPe4EdLpfV/Pu3KJ9eDZZ8O94EsyB9Xq1NrSnufPK4qg==" />

        <script>
    dataLayer = [{"SiteVer": "US", "MainOrUser": "UserPage", "PreOrFre": "Free", "Language": "en", "Culture": "en-US", "Instrumentation": "ShowPage", "Market": "DK", "ExpQS": ""}];
        </script>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-2MMH"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function (w, d, s, l, i) {
                {
                    w[l] = w[l] || [];
                    w[l].push({'gtm.start':
                                new Date().getTime(), event: 'gtm.js'});
                    var f = d.getElementsByTagName(s)[0],
                            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                            '//www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                }
            })(window, document, 'script', 'dataLayer', 'GTM-2MMH');</script>
        <!-- End Google Tag Manager -->

        <!-- Remove after blog exp concludes -->


        <script type="text/javascript" src="//css.simplesite.com/c/js/frontendApp.min.js?_v=0cb24ec3172a795bf8148823601f53ad"></script>
        <script type="text/javascript">if (typeof window.jQuery == "undefined") {
                (function () {
                    var a = document.createElement("script");
                    a.type = "text/javascript";
                    a.src = "/c/js/version3/frontendApp/init/frontendApp.min.js?_v=0cb24ec3172a795bf8148823601f53ad";
                    document.getElementsByTagName('head')[0].appendChild(a);
                })();
            }</script>

        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?render=explicit"></script>


        <script type='text/javascript'>
            var req = {"cmd": "VisitorInfo"};
            var theApiUrl = '/userPages/pages/handleAsyncCmd.aspx';
            window.session = {
                options: {gapi_location: true},
                start: function (session) {
                    req.sessionData = JSON.stringify(session);
                    $.ajax({
                        'url': theApiUrl,
                        'cache': false,
                        'type': 'POST',
                        'contentType': 'application/json; charset=utf-8',
                        'dataType': 'json',
                        'data': JSON.stringify(req)
                    });
                }
            }
        </script>
        <script type="text/javascript">
        </script>

    </body>
</html>