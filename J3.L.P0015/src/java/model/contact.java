/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author
 */
public class contact {

    String name;
    String address;
    String tel;
    String email;

    public contact() {
    }

    public contact(String name, String address, String tel, String email) {
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "contact{" + "name=" + name + ", address=" + address + ", tel=" + tel + ", email=" + email + '}';
    }

    public contact getContact() throws SQLException {
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from contact");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            name = rs.getString("name");
            address = rs.getString("address");
            tel = rs.getString("tel");
            email = rs.getString("email");
            contact x = new contact(name, address, tel, email);
            return x;
        }
        return null;
    }

    public static void main(String[] args) throws SQLException {
        contact c = new contact();
        System.out.println(c.getContact());
    }
}
