USE [J3.L.P0013]
GO
/****** Object:  Table [dbo].[contact]    Script Date: 11/2/2019 7:40:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[name] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[tel] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[home]    Script Date: 11/2/2019 7:40:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[home](
	[id] [int] NULL,
	[name] [nvarchar](50) NULL,
	[detail] [nvarchar](max) NULL,
	[image] [nvarchar](max) NULL,
	[role] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[product]    Script Date: 11/2/2019 7:40:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[id] [int] NULL,
	[name] [nvarchar](50) NULL,
	[price] [nvarchar](50) NULL,
	[detail] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[contact] ([name], [address], [tel], [email]) VALUES (N'The Sushi Restaurant', N'New York, NY, USA', N'12345', N'your-email@your-email.com')
INSERT [dbo].[home] ([id], [name], [detail], [image], [role]) VALUES (1, N'Shuhi', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip
 ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue
 duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.', N'./Home/i282319414620330848._szw1280h1280_.jpg', N'backgrond')
INSERT [dbo].[home] ([id], [name], [detail], [image], [role]) VALUES (2, N'24 types of sushi rolls', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.', N'./Home/i282319414620340695._szw480h1280_.jpg', N'backgrond')
INSERT [dbo].[product] ([id], [name], [price], [detail]) VALUES (1, N'Claritas est etiam processus', N'15.00', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.')
INSERT [dbo].[product] ([id], [name], [price], [detail]) VALUES (2, N'Duis autem vel eum iriure dolor.', N'20.00', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.')
INSERT [dbo].[product] ([id], [name], [price], [detail]) VALUES (3, N'Eodem modo typi, qui nunc nobis videntur.', N'25.00', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.')
