/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author
 */
public class homePage {
    int id;
    String name;
    String detail;
    String image;
    String role;

    public homePage() {
    }

    public homePage(int id, String name, String detail, String image, String role) {
        this.id = id;
        this.name = name;
        this.detail = detail;
        this.image = image;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "homePage{" + "id=" + id + ", name=" + name + ", detail=" + detail + ", image=" + image + ", role=" + role + '}';
    }
    
    public ArrayList<homePage> getHome() throws SQLException {
         ArrayList<homePage> al = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from home");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
             id = rs.getInt("id");
            name = rs.getString("name");
            detail = rs.getString("detail");
            image = rs.getString("image");
            role = rs.getString("role");
            al.add(new homePage(id, name, detail, image, role));
        }
        return al;
    }
    
    public static void main(String[] args) throws SQLException {
        homePage h = new homePage();
        System.out.println(h.getHome());
    }
}
