    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author
 */
public class product {
    int id;
    String name;
    String price;
    String detail;

    public product() {
    }

    public product(int id, String name, String price, String detail) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.detail = detail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "product{" + "id=" + id + ", name=" + name + ", price=" + price + ", detail=" + detail + '}';
    }
    

    
    public ArrayList<product> getPro() throws SQLException {
        ArrayList<product> al = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from product");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
            id = rs.getInt("id");
            name = rs.getString("name");
            price = rs.getString("price");
            detail = rs.getString("detail");
            al.add(new product(id, name, price, detail));
        }
        return al;
    }
    
    public static void main(String[] args) throws SQLException {
        product p = new product();
        System.out.println(p.getPro());
    }
}
