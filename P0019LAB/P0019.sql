USE [LabP0019]
GO
/****** Object:  Table [dbo].[Services]    Script Date: 11/13/2019 19:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[name] [nvarchar](max) NOT NULL,
	[image] [nvarchar](max) NOT NULL,
	[detail] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Services] ([name], [image], [detail]) VALUES (N'The Balance &amp; Harmony of Body and Mind', N'./Therapy and Massage - www.simplesite.com_us-123health_files/i283445314541983971._szw480h1280_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquipex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate.&nbsp;')
INSERT [dbo].[Services] ([name], [image], [detail]) VALUES (N'Hot Stone Massage', N'./Therapy and Massage - www.simplesite.com_us-123health_files/i283445314494038717._szw480h1280_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquipex ea commodo consequat.&nbsp;odem modo typi, qui nunc nobis videntur.')
INSERT [dbo].[Services] ([name], [image], [detail]) VALUES (N'Therapeutic Massage', N'./Therapy and Massage - www.simplesite.com_us-123health_files/i283445314494040127._szw480h1280_.jpg', N'Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposueritlitterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.')
/****** Object:  Table [dbo].[PriceList]    Script Date: 11/13/2019 19:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceList](
	[name] [nvarchar](max) NOT NULL,
	[info] [nvarchar](max) NOT NULL,
	[price] [int] NOT NULL,
	[type] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[PriceList] ([name], [info], [price], [type]) VALUES (N'Lorem ipsum dolor sit amet', N'20 minutes', 11, N'Treatment')
INSERT [dbo].[PriceList] ([name], [info], [price], [type]) VALUES (N'Demonstraverunt lectores', N'40 minutes', 22, N'Treatment')
INSERT [dbo].[PriceList] ([name], [info], [price], [type]) VALUES (N'Eodem modo typi', N'1 hour', 33, N'Treatment')
INSERT [dbo].[PriceList] ([name], [info], [price], [type]) VALUES (N'Lorem ipsum dolor sit amet', N'30 minutes', 55, N'Treatment')
INSERT [dbo].[PriceList] ([name], [info], [price], [type]) VALUES (N'Lorem ipsum dolor sit amet', N'Demonstraverunt lectores', 111, N'Courses')
INSERT [dbo].[PriceList] ([name], [info], [price], [type]) VALUES (N'Eodem modo typi', N'Lorem ipsum dolor sit amet', 222, N'Courses')
/****** Object:  Table [dbo].[Post]    Script Date: 11/13/2019 19:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[name] [nvarchar](max) NOT NULL,
	[image] [nvarchar](max) NOT NULL,
	[detail] [nvarchar](max) NOT NULL,
	[role] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Post] ([name], [image], [detail], [role]) VALUES (N'null', N'./Welcome - www.simplesite.com_us-123health_files/i283445314524568867._szw1280h1280_.jpg', N'null', N'background')
INSERT [dbo].[Post] ([name], [image], [detail], [role]) VALUES (N'Aromatherapy Means "Treatment Using Scents"', N'./Welcome - www.simplesite.com_us-123health_files/i283445314524568923._rsw480h360_szw480h360_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', N'post')
INSERT [dbo].[Post] ([name], [image], [detail], [role]) VALUES (N'Hot Stone Massage - A Variation On Classic Massage Therapy', N'./Welcome - www.simplesite.com_us-123health_files/i283445314491420783._rsw480h360_szw480h360_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.', N'post')
INSERT [dbo].[Post] ([name], [image], [detail], [role]) VALUES (N'Massage - Effect On Both Body And Mind', N'./Welcome - www.simplesite.com_us-123health_files/i283445314489630203._rsw480h360_szw480h360_.jpg', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', N'post')
/****** Object:  Table [dbo].[Employees]    Script Date: 11/13/2019 19:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[image] [nvarchar](max) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[detail] [nvarchar](max) NOT NULL,
	[role] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Employees] ([image], [name], [detail], [role]) VALUES (N'./Employees - www.simplesite.com_us-123health_files/i283445314494044460._szw480h1280_.jpg', N'Charlotte Robert', N'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', N'Masseur and sports masseur<br>(Medical and physiotherapist examined)')
INSERT [dbo].[Employees] ([image], [name], [detail], [role]) VALUES (N'./Employees - www.simplesite.com_us-123health_files/i283445314494044470._szw480h1280_.jpg', N'Louise Richard', N'Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.', N'Aroma therapist')
/****** Object:  Table [dbo].[Course]    Script Date: 11/13/2019 19:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Course](
	[name] [nvarchar](max) NOT NULL,
	[detail] [nvarchar](max) NOT NULL,
	[date] [date] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Course] ([name], [detail], [date]) VALUES (N'Introductory Massage', N'This subject is an excellent introduction to the basic concepts of massage including posture, draping, positioning, hygiene and hand techniques. It is a great way of helping you to decide on your dream career - it can really be an inspiration! Or, you may just want to learn something new to practice on family and friends!', CAST(0x74370B00 AS Date))
INSERT [dbo].[Course] ([name], [detail], [date]) VALUES (N'Certificate Course', N'We use a unique flexible training system to maximise the value you get from your learning. As well as meeting all government guidelines, our training allows you to learn at your own pace by choosing part-time, full-time or correspondence', CAST(0x7E370B00 AS Date))
/****** Object:  Table [dbo].[contact]    Script Date: 11/13/2019 19:11:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[name] [nvarchar](max) NOT NULL,
	[address] [nvarchar](max) NOT NULL,
	[city] [nvarchar](max) NOT NULL,
	[country] [nvarchar](max) NOT NULL,
	[tel] [nvarchar](max) NOT NULL,
	[email] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[contact] ([name], [address], [city], [country], [tel], [email]) VALUES (N'Aromatherapy - Massage', N'69 Abc', N'BangKok', N'Thailand', N'12345', N'your-email@your-email.com')
