/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author HaiLong
 */
public class Service {
    String name;
    String image;
    String detail;

    public Service() {
    }

    public Service(String name, String image, String detail) {
        this.name = name;
        this.image = image;
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
    
    public ArrayList<Service> getService() throws SQLException {
        ArrayList<Service> al = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from Services");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
            name = rs.getString("name");
            image = rs.getString("image");
            detail = rs.getString("detail");
            al.add(new Service(name, image, detail));
        }
        return al;
    }
}
