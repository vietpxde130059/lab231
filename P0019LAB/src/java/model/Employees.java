/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author HaiLong
 */
public class Employees {
    String image;
    String name;
    String detail;
    String role;

    public Employees() {
    }

    public Employees(String image, String name, String detail, String role) {
        this.image = image;
        this.name = name;
        this.detail = detail;
        this.role = role;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public ArrayList<Employees> getEmp() throws SQLException {
        ArrayList<Employees> al = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from Employees");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
            image = rs.getString("image");
            name = rs.getString("name");
            detail = rs.getString("detail");
            role = rs.getString("role");
            al.add(new Employees(image, name, detail, role));
        }
        return al;
    }
}
