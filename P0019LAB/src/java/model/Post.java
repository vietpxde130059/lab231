/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author HaiLong
 */
public class Post {
   String name,image,detail,role;

    public Post() {
    }

    public Post(String name, String image, String detail, String role) {
        this.name = name;
        this.image = image;
        this.detail = detail;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
   
   public ArrayList<Post> getPost() throws SQLException {
        ArrayList<Post> al = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from Post");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
            name = rs.getString("name");
            image = rs.getString("image");
            detail = rs.getString("detail");
            role = rs.getString("role");
            al.add(new Post(name, image, detail, role));
            
        }
        return al;
   }
}
