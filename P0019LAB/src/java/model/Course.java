/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author HaiLong
 */
public class Course {
    String name,detail;
    Date date;

    public Course() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Course(String name, String detail, Date date) {
        this.name = name;
        this.detail = detail;
        this.date = date;
    }
    
    public ArrayList<Course> getCourse() throws SQLException {
        ArrayList<Course> al = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from Course order by date DESC");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
            name = rs.getString("name");
            detail = rs.getString("detail");
            date = rs.getDate("date");
            al.add(new Course(name, detail, date));
        }
        return al;
    }
}
