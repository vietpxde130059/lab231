/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author HaiLong
 */
public class PriceList {
    String name;
    String info;
    int price;
    String type;

    public PriceList() {
    }

    public PriceList(String name, String info, int price, String type) {
        this.name = name;
        this.info = info;
        this.price = price;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public ArrayList<PriceList> getPriceList() throws SQLException {
        ArrayList<PriceList> al = new ArrayList<>();
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from PriceList");
        ResultSet rs = stmt.executeQuery();
        while(rs.next()) {
            name = rs.getString("name");
            info = rs.getString("info");
            price = rs.getInt("price");
            type = rs.getString("type");
            al.add(new PriceList(name, info, price, type));
                    
        }
        return al;
    }
}
