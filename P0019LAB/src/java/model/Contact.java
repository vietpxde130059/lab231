/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author
 */
public class Contact {

    String name, address, city, country, tel, email;

    public Contact() {
    }

    public Contact(String name, String address, String city, String country, String tel, String email) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.country = country;
        this.tel = tel;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Contact getContact() throws SQLException {
        Connection con = ConnectDB.getConnection();
        PreparedStatement stmt = con.prepareStatement("Select * from contact");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            name = rs.getString("name");
            address = rs.getString("address");
            city = rs.getString("city");
            country = rs.getString("country");
            tel = rs.getString("tel");
            email = rs.getString("email");
            Contact x = new Contact(name, address, city, country, tel, email);
            return x;
        }
        return null;
    }
}
